package support;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverInit {
    public static WebDriver driver;
    final String baseURL = "http://spree-vapasi.herokuapp.com/";
    final String browser = System.getProperty("environment");

    public void getBrowser() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(baseURL);
    }

    public void closeBrowser(){
        driver.quit();
    }

    public WebDriver getDriver(){
        return this.driver;
    }
}
