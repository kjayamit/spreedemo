package tests;

import org.testng.annotations.*;

import pages.HomePage;
import pages.ProductDetailsPage;
import pages.SearchResultPage;
import support.DriverInit;

public class AddToCartTest {
    HomePage homePage = new HomePage();
    SearchResultPage searchResultPage = new SearchResultPage();
    ProductDetailsPage productDetailsPage = new ProductDetailsPage();
    DriverInit driverInit;

    @BeforeMethod(alwaysRun = true)
    public void launchBrowser(){
        driverInit = new DriverInit();
        driverInit.getBrowser();
    }

    @Test (groups = { "Regression", "sanity" })
    public void validateAddToCart() {
        homePage.openFirstItem();
        productDetailsPage.addToCart();
    }


    @AfterMethod(alwaysRun = true)
    public void closeBrowser(){
        driverInit.closeBrowser();
    }
}
