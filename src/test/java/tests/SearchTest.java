package tests;

import org.testng.annotations.*;

import pages.HomePage;
import pages.SearchResultPage;
import support.DriverInit;

public class SearchTest {

    HomePage homePage = new HomePage();
    SearchResultPage searchResultPage = new SearchResultPage();
    DriverInit driverInit;

    @BeforeMethod(alwaysRun = true)
    public void launchBrowser(){
        driverInit = new DriverInit();
        driverInit.getBrowser();
    }

    @Test (groups = { "Regression", "sanity" })
    public void validateSearchText() {
        homePage.searchItem("Tote");
        searchResultPage.checkSearchResult("Ruby on Rails Tote");
    }

    @Test (groups = { "Regression" })
    public void validateFailedSearchText(){
        homePage.searchItem("Pen");
        searchResultPage.checkFailedSearchResult("No products found");
    }


    @AfterMethod(alwaysRun = true)
    public void closeBrowser(){
        driverInit.closeBrowser();
    }
}
