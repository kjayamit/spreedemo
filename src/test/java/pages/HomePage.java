package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import support.DriverInit;

public class HomePage {
    DriverInit driverInit = new DriverInit();

    public void searchItem(String ItemName){
        driverInit.getDriver().findElement(By.id("keywords")).sendKeys(ItemName);
        driverInit.getDriver().findElement(By.className("btn")).click();
    }

    public void openFirstItem(){
        driverInit.getDriver().findElement(By.xpath("//span[@title='Ruby on Rails Tote']")).click();
    }
}
