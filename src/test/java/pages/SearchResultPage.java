package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import support.DriverInit;

public class SearchResultPage {

    DriverInit driverInit = new DriverInit();
    WebElement searchItem;

    public void checkSearchResult(String ExpectedItem){
        searchItem = driverInit.getDriver().findElement(By.xpath("//span[contains(text(),'Ruby on Rails Tote')]"));
        //"//span[@itemprop='name']"));
        Assert.assertEquals(searchItem.getText(),ExpectedItem);
    }
    public void checkFailedSearchResult(String ExpectedItem){
        searchItem = driverInit.getDriver().findElement(By.xpath("//h6[contains(text(),'No products found')]"));
        Assert.assertEquals(searchItem.getText(),ExpectedItem);
    }
}
